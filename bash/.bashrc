#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '
alias vhs='mpv http://baud.vision:21225/vcr'
alias v='nvim'
alias unimatrix='unimatrix -l k'
alias u='sudo pacman -Syu'

# Netowrk mounts
alias msync='sudo mount 10.0.1.54:/mnt/NAS/syncthing_dataset /home/jsatkowski/syncthing_dataset'
alias usync='sudo umount 10.0.1.54:/mnt/NAS/syncthing_dataset /home/jsatkowski/syncthing_dataset'
alias mbac='sudo mount 10.0.1.54:/mnt/NAS/Backup_dataset /home/jsatkowski/Backup_dataset'
alias ubac='sudo umount 10.0.1.54:/mnt/NAS/Backup_dataset /home/jsatkowski/Backup_dataset'
alias mvid='sudo mount 10.0.1.54:/mnt/NAS/Video_dataset /home/jsatkowski/Video_dataset'
alias uvid='sudo umount 10.0.1.54:/mnt/NAS/Video_dataset /home/jsatkowski/Video_dataset'





# Command Prompt Custom Look
# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

#if [ -n "$force_color_prompt" ]; then
#    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
#	color_prompt=yes
#    else
#	color_prompt=
#    fi
#fi

#if [ "$color_prompt" = yes ]; then
#    PS1="\[\033[0;31m\]\342\224\214\342\224\200\$([[ \$? != 0 ]] && echo \"[\[\033[0;31m\]\342\234\227\[\033[0;37m\]]\342\224\200\")[$(if [[ ${EUID} == 0 ]]; then echo '\[\033[01;31m\]root\[\033[01;33m\]@\[\033[01;96m\]\h'; else echo '\[\033[0;39m\]\u\[\033[01;33m\]@\[\033[01;96m\]\h'; fi)\[\033[0;31m\]]\342\224\200[\[\033[0;32m\]\w\[\033[0;31m\]]\n\[\033[0;31m\]\342\224\224\342\224\200\342\224\200\342\225\274 \[\033[0m\]\[\e[01;33m\]\\$\[\e[0m\]"
#else
#   PS1='┌──[\u@\h]─[\w]\n└──╼ \$ '
#fi

### Powerline-Shell ###

function _update_ps1() {
    PS1=$(powerline-shell $?)
}

if [[ $TERM != linux && ! $PROMPT_COMMAND =~ _update_ps1 ]]; then
    PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
fi

### Powerline-Shell ###


# Created by `pipx` on 2024-05-05 10:45:23
export PATH="$PATH:/home/jsatkowski/.local/bin"
export EDITOR='nvim'
export VISUAL='nvim'
