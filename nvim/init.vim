 set relativenumber

 call plug#begin()

" List your plugins here
Plug 'vim-airline/vim-airline'

call plug#end()

source $HOME/.config/nvim/themes/airline.vim
